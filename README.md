# README

  This project is the implementation of Boggle Game in Ruby on Rails with React.

- Ruby version

   This project is created with ruby version 2.6.5 and rails version 6.0.2.2. The project is created with webpack and react.

    -rails new boggle-game --webpack=react

    Added gem 'react-rails' for running react app in Gemfile
    Updated Gemfile to use sqlite version ~1.4.2

    Once bundle install completed. We can run following commands so that we can add react to our application and create react component
    -rails webpacker:install:react  
    -rails generate react:install yarn install

    Added gem 'rspec-rails' for testing.

    Our application logic for Boggle are in /services folder inside app/ folder and for testing code /services folder 
    is created inside test/ folder. For testing our first Boggle Board creation you can run command as:
    -rspec .\test\services\board_test..rb

    We are going to solve the boggle puzzle by implementing the Trie data structure which is used for searching if the string
    formed using DFS is present in the list of words inserted into it. Trie helps us in optimizing the searching of the word since
    we will create prefix tree and only search through sub-string which can form word and other sub-string that cannot form word are not
    traversed. For testing our Trie implementation you can run command as:
    -rspec .\test\services\trie_test..rb
    
    We will create prefix tree from the words in dictionary.txt file that is inside db/ folder.
