Rails.application.routes.draw do
  namespace :v1, defaults: { format: 'json'} do
    get 'boggle/board', to: 'boggle#board'
    post 'boggle/addword', to: 'boggle#addword'
    get 'boggle/score', to: 'boggle#score'

  end  
  root 'home#index'
end

