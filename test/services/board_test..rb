require 'rspec'
require_relative '../../app/services/Board'

describe Board do
    it "should create board of given size" do
        board = Board.new
        expect(board.generate_board(4).size).to eq 4
        expect(board.generate_board(5).size).to eq 5
    end
end
