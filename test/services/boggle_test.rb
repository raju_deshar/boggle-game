require 'rspec'
require_relative '../../app/services/Boggle'
describe Boggle do
    board =
        [
            ['E', 'A', 'C','E'], 
            ['P', 'A', 'R','I'], 
            ['H', 'N', 'T','S'],
            ['O', 'M', 'E','O']          
        ]
    boggle = Boggle.new(board)

    it "should find the adjacent letter's co-ordinates of the board" do
        expect(boggle.get_neighbour(0,0)).to eq [[0, 0], [0, 1], [1, 0], [1, 1]]
        expect(boggle.get_neighbour(0,1)).to eq [[0, 0], [0, 1], [0, 2], [1, 0], [1, 1], [1, 2]]
        expect(boggle.get_neighbour(0,2)).to eq [[0, 1], [0, 2], [0, 3], [1, 1], [1, 2], [1, 3]]
        expect(boggle.get_neighbour(0,3)).to eq [[0, 2], [0, 3], [1, 2], [1, 3]]
        expect(boggle.get_neighbour(1,1)).to eq [[0, 0], [0, 1], [0, 2], [1, 0], [1, 1], [1, 2], [2, 0], [2, 1], [2, 2]]
    end

    it "should validate correct boggle words for given board" do
        solution = boggle.get_result
        expect(solution.include?('part')).to eq true
        expect(solution.include?('home')).to eq true
        expect(solution.include?('toe')).to eq true
        expect(solution.include?('met')).to eq true
        expect(solution.include?('pan')).to eq true
        expect(solution.include?('phone')).to eq true
        expect(solution.include?('sir')).to eq true
        expect(solution.include?('stem')).to eq true
        expect(solution.include?('name')).to eq false
        expect(solution.include?('nabe')).to eq false
    end
end