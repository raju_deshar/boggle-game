require 'rspec'
require_relative '../../app/services/Trie'

describe Trie do
    trie = Trie.new
    trie.add_word('CAN')

    it "should add word and create prefix tree with valid word at end" do        
        expect(trie.root.children['C'].data).to eq 'C'
        expect(trie.root.children['C'].children['A'].data).to eq 'CA'
        expect(trie.root.children['C'].children['A'].children['N'].data).to eq 'CAN'
        expect(trie.root.children['C'].children['A'].children['N'].term).to eq true
    end

    it "should get child node" do
        expect(trie.root.children['C'].get_child('A')).to eq trie.root.children['C'].children['A']
    end

    it "should have child node of type Node" do
        expect(trie.root.children['C'].get_child('A').instance_of? Node).to eq true
    end

   
end