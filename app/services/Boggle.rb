require_relative "Trie"

class Boggle
    @@solution = []
    @@foundWord = []
    attr_accessor :board
    attr_reader :score

    def initialize(board)
        @board =  board            
        @dict = Hash.new
    end

    def assign_letter_row_col
        for i in 0..board.size-1
            for j in 0..board.size-1
                @dict[[i,j]] = @board[i][j]
            end
        end        
    end

    def get_neighbour(row,col)
        neighbours = []
        for i in row-1..row+1
            for j in col-1..col+1
                if i >= 0 && j >= 0 && i < board.size && j < board.size 
                    neighbours << [i,j]
                end
            end
        end
        return neighbours
    end

    def solution
        if(@@solution.size === 0)
            @@solution = get_result()
        end
        answers =  @@solution.uniq
        @@solution = []
        return answers
    end

    def get_word(row, col, visited_coordinates, prefix_node)
        neighbours = get_neighbour(row,col)
        if prefix_node != ''
            if prefix_node.term
                @@solution << prefix_node.data
            end
        end
        visited_coordinates[[row,col]]=true
        for i in neighbours
            if !visited_coordinates[i]
                if prefix_node != ''
                    letter = @dict[[row,col]].to_s.downcase.gsub(/\s+/, '')
                    child_node = prefix_node.get_child(letter)
                    get_word(i[0], i[1], visited_coordinates, child_node)
                end              
            end
        end
        visited_coordinates[[row,col]]=false
    end


    def get_result
        assign_letter_row_col()
        trie = Trie.new
        words = File.read("db/dictionary.txt").split
        words.map do |word|
            if word.length > 2
                trie.add_word(word)
            end
        end
        for i in 0..board.size
            for j in 0..board.size
                get_word(i, j,{}, trie.root)
            end
        end
        return solution()
    end

    
    def add_word_score(word)
        if @@solution.size === 0
            @@solution = get_result()
        end
        if @@solution.include?(word)
            @@foundWord << word
            return true, word.length
        else
            return false, 0
        end
    end 
    
    def get_score
        game_score = 0
        for word in @@foundWord.uniq
            game_score += word.size
        end
        return game_score, @@foundWord, solution()
    end
end

