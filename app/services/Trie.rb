class Node
    attr_reader :data, :children
    attr_accessor :term
    def initialize(data)
      @data = data
      @children = Hash.new
      @term = false
    end
  
    def add_child(char)
      if @data.nil?
        @data = ''
      end
      child = Node.new(@data + char)
      @children[char] = child
      return child
    end
  
    def get_child(char)
      if @children.key?(char)
        return @children[char]
      else
        return ''
      end
    end
  end
  
  class Trie
    attr_reader :root
    def initialize
      @root = Node.new(nil)
    end
  
    def add_word(word)
      node = @root
      while word != ''
        first_letter = word[0]
        remainder = word[1..-1]
        child = node.get_child(first_letter)
        if child == ''
          child = node.add_child(first_letter)
        end
        if remainder.empty?
          child.term = true
        end
        node = child
        word = remainder
      end      
    end
  end


 