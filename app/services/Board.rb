class Board
    @@board_matrix = [
        ['A','A','I','F','R','S'],
        ['A','A','E','E','O','E'],
        ['R','A','F','I','R','S'],
        ['A','D','E','N','N','N'],
        ['P','E','O','E','E','M'],
        ['A','E','E','G','M','U'],
        ['A','E','G','M','N','N'],
        ['A','F','I','R','S','Y'],
        ['B','J','K','Q','X','Z'],
        ['C','C','N','S','T','W'],
        ['C','E','I','I','L','T'],
        ['C','E','I','L','P','T'],
        ['C','E','I','P','S','T'],
        ['D','D','L','N','O','R'],
        ['D','H','H','L','O','R'],
        ['D','H','H','N','O','T'],
        ['D','H','L','N','O','R'],
        ['E','I','I','I','T','T'],
        ['E','M','O','T','T','T'],
        ['E','N','S','S','S','U'],
        ['F','I','P','R','S','Y'],
        ['G','O','R','R','V','W'],
        ['H','I','P','R','R','Y'],
        ['N','O','O','T','U','W'],
        ['O','O','O','T','T','U']
    ]

    def generate_board(x)
        board = []
        while board.length< x do
            index = rand(@@board_matrix.length)
            board << @@board_matrix[index][0..x-1]
        end
        return board
    end
end
