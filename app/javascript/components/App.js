import React from "react";
import Header from "./HeaderPage";
import BogglePage from "./BogglePage";

class App extends React.Component {
  render() {
    return (
      <div className="container-fluid">
        <Header />
        <BogglePage />
      </div>
    );
  }
}
export default App;
