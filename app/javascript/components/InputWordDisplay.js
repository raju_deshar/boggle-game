import React from "react";

const InputWordDisplay = (props) => (
  <ul>
    {props.items.map((item, index) => (
      <li className="list-group-item" key={index}>
        {item}
      </li>
    ))}
  </ul>
);

export default InputWordDisplay;
