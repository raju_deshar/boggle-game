import React, { Component } from "react";
import InputWordDisplay from "./InputWordDisplay";

class BoggleWordInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: "",
      words: [],
      start: this.props.start,
    };
    this.addWord = this.addWord.bind(this);
  }

  onChange = (event) => {
    this.setState({ word: event.target.value });
  };

  addWord = (event) => {
    event.preventDefault();
    this.addWordApi();
  };

  addWordApi() {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ word: this.state.word }),
    };
    fetch("v1/boggle/addword", requestOptions)
      .then((response) => response.json())
      .then((response) => {
        if (!response.result[0]) {
          alert("Sorry the word you entered is not valid!");
        } else {
          this.setState({
            word: "",
            words: [...this.state.words, this.state.word.toLowerCase()],
          });
        }
      });
  }

  render() {
    if (this.state.start) {
      return (
        <div>
          <label htmlFor="boggle">
            <b>Your Boggle Words Here:</b>
          </label>
          <div className="form-group form-inline boggle-input-main">
            <input
              type="text"
              name="boggle"
              className="form-control boggle-input"
              placeholder="Please insert the boggle word"
              value={this.state.word}
              onChange={this.onChange}
            />
            &nbsp;
            <span className="input-group-btn">
              <button onClick={this.addWord} className="btn btn-primary">
                Add Word
              </button>
            </span>
          </div>
          <InputWordDisplay items={this.state.words} />
        </div>
      );
    }
  }
}

export default BoggleWordInput;
