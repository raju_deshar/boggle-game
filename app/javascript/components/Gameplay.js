import React from "react";
class Gameplay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      minutes: 3,
      seconds: 0,
      start: this.props.start,
    };
    this.startCountDown = this.startCountDown.bind(this);
  }

  stopGamePlay() {
    this.getResult();
  }

  getResult() {
    fetch("v1/boggle/score")
      .then((response) => response.json())
      .then((data) => this.props.onGameStop(data));
  }

  startCountDown() {
    this.resetBoggleBoard();
    this.myInterval = setInterval(() => {
      const { seconds, minutes, start } = this.state;
      this.setState({
        start: true,
      });

      if (seconds > 0) {
        this.setState(({ seconds }) => ({
          seconds: seconds - 1,
        }));
      }
      if (seconds === 0) {
        if (minutes === 0) {
          this.stopGamePlay();
          clearInterval(this.myInterval);
        } else {
          this.setState(({ minutes }) => ({
            minutes: minutes - 1,
            seconds: 59,
          }));
        }
      }
    }, 1000);
  }
  resetBoggleBoard = () => {
    const url = "v1/boggle/board";
    fetch(url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("Network response was not ok.");
      })
      .then((response) => this.props.onCreateBoard(response.letters))
      .catch(() => alert(""));
  };

  componentDidMount() {
    if (this.props.start) {
      this.startCountDown();
    }
  }

  componentWillUnmount() {
    clearInterval(this.myInterval);
  }

  render() {
    const { minutes, seconds, start } = this.state;
    if (start) {
      return (
        <div>
          {minutes === 0 && seconds === 0 ? (
            <h2>Game Stopped</h2>
          ) : (
            <h1>
              Time Remaining: {minutes}:{seconds < 10 ? `0${seconds}` : seconds}
            </h1>
          )}
        </div>
      );
    } else {
      return (
        <div style={{ marginTop: 14 }}>
          <button
            className="btn btn-success"
            disabled={!this.props.start}
            onClick={this.startCountDown}
          >
            Play Again
          </button>
        </div>
      );
    }
  }
}

export default Gameplay;
