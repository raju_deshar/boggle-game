import React from "react";
const Header = () => {
  const [showInfo, setShowInfo] = React.useState(false);
  const onClick = () => setShowInfo(true);
  const hideInfo = () => setShowInfo(false);
  return (
    <div>
      <div className="jumbotron header">
        <h1>Boggle Game</h1>
        <button className="btn btn-primary btn-sm" onClick={onClick}>
          For game info click here
        </button>
      </div>
      {showInfo ? <Modal onClick={hideInfo} /> : null}
    </div>
  );
};

const Modal = ({ onClick }) => {
  return (
    <div className="show-modal-game">
      <div className="modal-content-info">
        <p>
          Boggle Board of 4x4 is generated each time a new game is played. You
          have to make up as much of words you can from the letters in the
          board. You can only choose the adjacent letters to make up the words.
          For the invalid entry of word error message is shown. For all the
          correct words you entered are given the score. The score points is
          length of the word. <br />
          Good luck have a nice gameplay.
        </p>
        <button className="btn btn-dark sm" onClick={onClick}>
          Close
        </button>
      </div>
    </div>
  );
};

export default Header;
