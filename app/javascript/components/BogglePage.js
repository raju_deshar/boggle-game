import React from "react";
import BoggleWordInput from "./BoggleWordInput";
import Gameplay from "./Gameplay";
class BogglePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      start: false,
      board: [],
      answers: [],
      showresult: false,
    };

    this.createBoggleBoard = this.createBoggleBoard.bind(this);
  }

  createBoggleBoard() {
    const url = "v1/boggle/board";
    fetch(url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("");
      })
      .then((response) =>
        this.setState({ board: response.letters, start: true })
      )
      .catch(() => alert("Error "));
  }

  stopGamePlay = (answers) => {
    this.setState({ start: false, showresult: true, answers: answers });
  };

  resetBoggleBoard = (board) => {
    this.setState({ board: board });
  };

  render() {
    const { board, start, answers, showresult } = this.state;

    if (start) {
      return (
        <div className="col-md-12 row">
          <div className="col-md-5">
            <BoggleWordInput start={start} />
          </div>
          <div className="col-md-4">
            {board.map((row, i) => (
              <div className="board">
                <div className="row">
                  {row.map((col, j) => (
                    <div className="boggle">
                      <span key={i.toString() + j.toString()}>{col}</span>
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
          <div className="col-md-3">
            <Gameplay
              start={start}
              onCreateBoard={this.resetBoggleBoard}
              onGameStop={this.stopGamePlay}
            />
          </div>
        </div>
      );
    } else {
      if (showresult) {
        return (
          <div>
            <div className="row justify-content-md-center">
              <button
                onClick={this.createBoggleBoard}
                className="btn btn-primary"
              >
                Play
              </button>
            </div>
            <p style={{ marginTop: 14 }}>
              <b>All the words in Boggle are:</b>
            </p>
            <div className="row col-md-12" style={{ marginTop: 10 }}>
              {answers.result[2].map((item, index) => (
                <li key={index} className="list-group-item">
                  {item}
                </li>
              ))}
            </div>
            <div className="col-md-12" style={{ marginTop: 10 }}>
              <p>
                <b>All the correct words you found out:</b>
              </p>{" "}
              <br />
              {answers.result[1].map((item, index) => (
                <li key={index} className="list-group-item">
                  {item}
                </li>
              ))}
            </div>
            <div className="col-md-12" style={{ marginTop: 10 }}>
              <p>
                Your Score:<b> {answers.result[0]}</b>
              </p>
            </div>
          </div>
        );
      } else {
        return (
          <div className="row justify-content-md-center">
            <button
              onClick={this.createBoggleBoard}
              className="btn btn-primary"
            >
              Play
            </button>
          </div>
        );
      }
    }
  }
}

export default BogglePage;
