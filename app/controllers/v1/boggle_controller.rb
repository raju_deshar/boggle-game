require_relative '../../services/Board'

class V1::BoggleController<ApplicationController
    skip_before_action :verify_authenticity_token
    
    def index
    end

    def board
        board = Board.new
        board_letters =  board.generate_board(4)
        session[:board_letters] = board_letters
        render json: {:letters=>
            board_letters
        }.to_json
    end

    def addword
        word = params[:word]
        board = session[:board_letters]
        boggle = Boggle.new(board)
        result = boggle.add_word_score(word.to_s.downcase.gsub(/\s+/, ''))        
        render json:{ :result=>
            result
        }.to_json
    end

    def score
        board = session[:board_letters]
        boggle = Boggle.new(board)
        result = boggle.get_score
        render json: {:result=>
            result
        }.to_json
    end

end